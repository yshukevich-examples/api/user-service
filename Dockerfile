FROM registry.gitlab.com/yshukevich-examples/infrastructure/template-java:1.0.67
VOLUME /tmp
COPY target/userservice*.jar app.jar
ENTRYPOINT ["java","-XX:+UseG1GC","-Xmx360m", "-Xms256m", "-jar","/app.jar"]
