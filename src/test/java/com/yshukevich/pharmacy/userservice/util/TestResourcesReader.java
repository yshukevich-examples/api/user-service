package com.yshukevich.pharmacy.userservice.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestResourcesReader <T> {

    @SneakyThrows
    public T readData(String filePath, Class<T> className){
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream inputStream= classLoader.getResourceAsStream(filePath);
        return new ObjectMapper().findAndRegisterModules().readValue(inputStream, className);
    }

    @SneakyThrows
    public List<T> readDataList(String filePath, Class<T> className){
        ClassLoader classLoader = this.getClass().getClassLoader();
        InputStream inputStream= classLoader.getResourceAsStream(filePath);
        String jsonString = "";
        if(inputStream!=null) {
             jsonString = new BufferedReader(
                    new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                    .lines()
                    .collect(Collectors.joining("\n"));
        }
        return new ObjectMapper().findAndRegisterModules().readerForListOf(className).readValue(jsonString);
    }

}
