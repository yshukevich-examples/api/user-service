package com.yshukevich.pharmacy.userservice.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yshukevich.pharmacy.userservice.controllers.AuthController;
import com.yshukevich.pharmacy.userservice.controllers.ReadUserController;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.models.security.LoginRequest;
import com.yshukevich.pharmacy.userservice.security.PermissionsToAuthoritiesMapper;
import com.yshukevich.pharmacy.userservice.security.WebSecurityConfig;
import com.yshukevich.pharmacy.userservice.security.jwt.AuthEntryPointJwt;
import com.yshukevich.pharmacy.userservice.security.jwt.AuthTokenFilter;
import com.yshukevich.pharmacy.userservice.security.jwt.JwtConfig;
import com.yshukevich.pharmacy.userservice.security.jwt.JwtUtils;
import com.yshukevich.pharmacy.userservice.security.services.SecurityUser;
import com.yshukevich.pharmacy.userservice.security.services.UserDetailsServiceImpl;
import com.yshukevich.pharmacy.userservice.service.AuthUserService;
import com.yshukevich.pharmacy.userservice.service.ReadUserService;
import com.yshukevich.pharmacy.userservice.util.TestResourcesReader;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import({
        WebSecurityConfig.class,
        JwtUtils.class,
        JwtConfig.class,
        AuthEntryPointJwt.class,
        AuthTokenFilter.class,
        UserDetailsServiceImpl.class,
        PermissionsToAuthoritiesMapper.class,
        SecurityUser.class
})
@WebMvcTest(ReadUserController.class)
@ActiveProfiles("test")
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    TestResourcesReader resourcesReader = new TestResourcesReader();

    @MockBean
    private ReadUserService readUserService;

    private UserCredentialsFullModel validUserModelV2;

    @Test
    void shouldAllowAccessWithUserAuthentication() throws Exception {
        validUserModelV2 = (UserCredentialsFullModel) resourcesReader.readData(
                "ExistingUserModel/ExistingValidUserModelV2.json",
                UserCredentialsFullModel.class);
        Mockito.when(readUserService.getFullUserCredentialsByLogin("ivan")).thenReturn(validUserModelV2);
        this.mockMvc
                .perform(
                        get("/user/getUserByLogin/{login}", "ivan")
                                .accept(MediaType.APPLICATION_JSON)
                                .with(jwt().authorities(new SimpleGrantedAuthority("Read a single user")))
                )
                .andExpect(status().is(200));
    }


    @Test
    void shouldNotAllowAccessWithoutUserAuthentication() throws Exception {
        this.mockMvc
                .perform(
                        get("/getUserByLogin")
                                .param("login", "ivan")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(404));
    }
}
