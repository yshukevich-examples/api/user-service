package com.yshukevich.pharmacy.userservice.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yshukevich.pharmacy.userservice.controllers.AuthController;
import com.yshukevich.pharmacy.userservice.models.security.LoginRequest;
import com.yshukevich.pharmacy.userservice.security.PermissionsToAuthoritiesMapper;
import com.yshukevich.pharmacy.userservice.security.WebSecurityConfig;
import com.yshukevich.pharmacy.userservice.security.jwt.AuthEntryPointJwt;
import com.yshukevich.pharmacy.userservice.security.jwt.AuthTokenFilter;
import com.yshukevich.pharmacy.userservice.security.jwt.JwtConfig;
import com.yshukevich.pharmacy.userservice.security.jwt.JwtUtils;
import com.yshukevich.pharmacy.userservice.security.services.SecurityUser;
import com.yshukevich.pharmacy.userservice.security.services.UserDetailsServiceImpl;
import com.yshukevich.pharmacy.userservice.service.AuthUserService;
import com.yshukevich.pharmacy.userservice.service.ReadUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Import({
        WebSecurityConfig.class,
        JwtUtils.class,
        JwtConfig.class,
        AuthEntryPointJwt.class,
        AuthTokenFilter.class,
        UserDetailsServiceImpl.class,
        PermissionsToAuthoritiesMapper.class,
        SecurityUser.class
})
@WebMvcTest(AuthController.class)
@ActiveProfiles("test")
class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthUserService authUserService;

    @MockBean
    private ReadUserService readUserService;

    @Test
    void shouldAllowAccessWithoutAuthentication() throws Exception {
        String requestBody = new ObjectMapper()
                .valueToTree(new LoginRequest("ivan", "password"))
                .toString();
        this.mockMvc
                .perform(
                        post("/login")
                                .content(requestBody)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is(200));
    }

}
