package com.yshukevich.pharmacy.userservice.unit;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.entities.dictionaries.CustomerTypeEntity;
import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRoleEntity;
import com.yshukevich.pharmacy.userservice.exceptions.UserProfileAlreadyExistsException;
import com.yshukevich.pharmacy.userservice.mapper.DictionaryMappingService;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserCredentialsMapperImpl;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserProfileMapperImpl;
import com.yshukevich.pharmacy.userservice.models.security.NewUserCredentialsModelWeb;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.CustomerTypeRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.UserRoleRepository;
import com.yshukevich.pharmacy.userservice.service.CheckUserService;
import com.yshukevich.pharmacy.userservice.service.CreateUserService;
import com.yshukevich.pharmacy.userservice.util.TestResourcesReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel.NOT_CUSTOMER;
import static com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel.RETAIL;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
public class CreateUserServiceTest {

    private NewUserCredentialsModelWeb validUserModelV1;

    private NewUserCredentialsModelWeb validUserModelV2;

    private NewUserCredentialsModelWeb validAdminModelV1;

    private UserCredentialsEntity validCredentialsEntityV1;

    private UserCredentialsEntity validCredentialsEntityV2;

    private UserCredentialsEntity validCredentialsNewEntityV2;

    private UserCredentialsEntity validAdminCredentialsNewEntityV1;

    private UserCredentialsEntity validAdminCredentialsEntityV1;

    private UserCredentialsEntity validCredentialsNewEntityV1;

    private UserRoleEntity userRoleEntity;

    private CustomerTypeEntity customerTypeEntity;

    private UserRoleEntity adminRoleEntity;

    private CustomerTypeEntity customerTypeNoneEntity;

    @InjectMocks
    private CreateUserService createUserService;

    @InjectMocks
    private UserCredentialsMapperImpl userCredentialsMapper;

    @InjectMocks
    private UserProfileMapperImpl userProfileMapper;

    @InjectMocks
    private DictionaryMappingService dictionaryMappingService;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    private UserCredentialsRepository userCredentialsRepository;

    @Mock
    private CheckUserService checkUserService;

    @Mock
    private CustomerTypeRepository customerTypeRepository;
    @Mock
    private UserRoleRepository userRoleRepository;

    @BeforeAll
    public void setUp(){
        TestResourcesReader resourcesReader = new TestResourcesReader();

        dictionaryMappingService = new DictionaryMappingService(customerTypeRepository, userRoleRepository);
        userProfileMapper = new UserProfileMapperImpl(dictionaryMappingService);
        userCredentialsMapper = new UserCredentialsMapperImpl(dictionaryMappingService, userProfileMapper);
        createUserService = new CreateUserService(userCredentialsRepository, checkUserService, userCredentialsMapper, encoder);

        userRoleEntity = (UserRoleEntity) resourcesReader.readData("Dictionaries/UserRoleEntityV1.json", UserRoleEntity.class);
        customerTypeEntity = (CustomerTypeEntity) resourcesReader.readData("Dictionaries/CustomerTypeEntityV1.json", CustomerTypeEntity.class);
        adminRoleEntity = (UserRoleEntity) resourcesReader.readData("Dictionaries/AdminRoleEntityV1.json", UserRoleEntity.class);
        customerTypeNoneEntity = (CustomerTypeEntity) resourcesReader.readData("Dictionaries/CustomerTypeNullV1.json", CustomerTypeEntity.class);

        validUserModelV1 = (NewUserCredentialsModelWeb) resourcesReader.readData("NewUserModel/NewValidUserModelV1.json", NewUserCredentialsModelWeb.class);
        validCredentialsNewEntityV1 = (UserCredentialsEntity) resourcesReader.readData("NewUserEntity/NewValidUserEntityV1.json", UserCredentialsEntity.class);
        validCredentialsEntityV1 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidUserEntityV1.json", UserCredentialsEntity.class);

        validUserModelV2 = (NewUserCredentialsModelWeb) resourcesReader.readData("NewUserModel/NewValidUserModelV2.json", NewUserCredentialsModelWeb.class);
        validCredentialsEntityV2 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidUserEntityV2.json", UserCredentialsEntity.class);
        validCredentialsNewEntityV2 = (UserCredentialsEntity) resourcesReader.readData("NewUserEntity/NewValidUserEntityV2.json", UserCredentialsEntity.class);

        validAdminCredentialsNewEntityV1 = (UserCredentialsEntity) resourcesReader.readData("NewUserEntity/NewValidAdminEntityV1.json", UserCredentialsEntity.class);
        validAdminCredentialsEntityV1 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidAdminEntityV1.json", UserCredentialsEntity.class);
        validAdminModelV1 = (NewUserCredentialsModelWeb) resourcesReader.readData("NewUserModel/NewValidAdminModelV1.json", NewUserCredentialsModelWeb.class);
    }

    @Test
    void saveValidUser() {
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validUserModelV1.getLogin())).thenReturn(false);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validUserModelV1.getPhone())).thenReturn(false);
        Mockito.when(userRoleRepository.findById(1)).thenReturn(Optional.ofNullable(userRoleEntity));
        Mockito.when(customerTypeRepository.findById(1)).thenReturn(Optional.ofNullable(customerTypeEntity));
        Mockito.when(encoder.encode(validUserModelV1.getPassword())).thenReturn(validUserModelV1.getPassword());
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validCredentialsEntityV1);
        var entity = createUserService.createUserCredentials(validUserModelV1);
        verify(userCredentialsRepository).save(validCredentialsNewEntityV1);
        assertEquals(1, entity.getId());
        assertEquals("ivan", entity.getLogin());
        assertNotNull(entity.getUserProfile());
        assertNull(entity.getUserProfile().getAddress());
        assertEquals(1, entity.getUserProfile().getId());
        assertEquals(RETAIL, entity.getUserProfile().getCustomerType());
    }

    @Test
    void saveValidUserV2() {
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validUserModelV2.getLogin())).thenReturn(false);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validUserModelV2.getPhone())).thenReturn(false);
        Mockito.when(userRoleRepository.findById(1)).thenReturn(Optional.ofNullable(userRoleEntity));
        Mockito.when(customerTypeRepository.findById(1)).thenReturn(Optional.ofNullable(customerTypeEntity));
        Mockito.when(encoder.encode(validUserModelV2.getPassword())).thenReturn(validUserModelV2.getPassword());
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validCredentialsEntityV2);
        var entity = createUserService.createUserCredentials(validUserModelV2);
        verify(userCredentialsRepository).save(validCredentialsNewEntityV2);
        assertEquals(1, entity.getId());
        assertEquals("ivan", entity.getLogin());
        assertNotNull(entity.getUserProfile());
        assertEquals("PL", entity.getUserProfile().getCountry());
        assertEquals(1, entity.getUserProfile().getId());
        assertEquals(RETAIL, entity.getUserProfile().getCustomerType());
    }

    @Test
    void saveAdminUser(){
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validAdminModelV1.getLogin())).thenReturn(false);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validAdminModelV1.getPhone())).thenReturn(false);
        Mockito.when(userRoleRepository.findById(3)).thenReturn(Optional.ofNullable(adminRoleEntity));
        Mockito.when(customerTypeRepository.findById(4)).thenReturn(Optional.ofNullable(customerTypeNoneEntity));
        Mockito.when(encoder.encode(validAdminModelV1.getPassword())).thenReturn(validAdminModelV1.getPassword());
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validAdminCredentialsEntityV1);
        var entity = createUserService.createAdminCredentials(validAdminModelV1);
        assertEquals(2, entity.getId());
        assertEquals("petr", entity.getLogin());
        assertNotNull(entity.getUserProfile());
        assertEquals(2, entity.getUserProfile().getId());
        assertEquals(NOT_CUSTOMER, entity.getUserProfile().getCustomerType());
        verify(userCredentialsRepository).save(validAdminCredentialsNewEntityV1);
    }

    @Test
    void saveAlreadyExistingUserByLogin(){
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validUserModelV2.getLogin())).thenReturn(true);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validUserModelV2.getPhone())).thenReturn(false);
        Mockito.when(userRoleRepository.findById(1)).thenReturn(Optional.ofNullable(userRoleEntity));
        Mockito.when(customerTypeRepository.findById(1)).thenReturn(Optional.ofNullable(customerTypeEntity));
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validCredentialsEntityV1);
        assertThrows(
                UserProfileAlreadyExistsException.class,
                () -> createUserService.createUserCredentials(validUserModelV1)
        );
    }

    @Test
    void saveAlreadyExistingUserByPhone(){
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validUserModelV2.getLogin())).thenReturn(false);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validUserModelV2.getPhone())).thenReturn(true);
        Mockito.when(userRoleRepository.findById(1)).thenReturn(Optional.ofNullable(userRoleEntity));
        Mockito.when(customerTypeRepository.findById(1)).thenReturn(Optional.ofNullable(customerTypeEntity));
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validCredentialsEntityV1);
        assertThrows(
                UserProfileAlreadyExistsException.class,
                () -> createUserService.createUserCredentials(validUserModelV1)
        );
    }

}
