package com.yshukevich.pharmacy.userservice.unit;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.entities.dictionaries.CustomerTypeEntity;
import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRoleEntity;
import com.yshukevich.pharmacy.userservice.mapper.DictionaryMappingService;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserCredentialsMapperImpl;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserProfileMapperImpl;
import com.yshukevich.pharmacy.userservice.models.ExistingUserCredentialsModelWeb;
import com.yshukevich.pharmacy.userservice.models.dictionaries.UserRoleModel;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.CustomerTypeRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.UserRoleRepository;
import com.yshukevich.pharmacy.userservice.service.CheckUserService;
import com.yshukevich.pharmacy.userservice.service.UpdateUserService;
import com.yshukevich.pharmacy.userservice.util.TestResourcesReader;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel.NOT_CUSTOMER;
import static com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel.RETAIL;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;

@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
public class UpdateUserServiceTest {

    @InjectMocks
    private UpdateUserService updateUserService;

    @InjectMocks
    private UserCredentialsMapperImpl userCredentialsMapper;

    @InjectMocks
    private UserProfileMapperImpl userProfileMapper;

    @InjectMocks
    private DictionaryMappingService dictionaryMappingService;

    @Mock
    private CheckUserService checkUserService;

    @Mock
    private UserCredentialsRepository userCredentialsRepository;

    @Mock
    private CustomerTypeRepository customerTypeRepository;

    @Mock
    private UserRoleRepository userRoleRepository;


    private UserRoleEntity userRoleEntity;

    private CustomerTypeEntity customerTypeEntity;

    private UserRoleEntity adminRoleEntity;

    private CustomerTypeEntity customerTypeNoneEntity;

    private ExistingUserCredentialsModelWeb validUserModelV3;

    private UserCredentialsEntity validCredentialsEntityV3;

    private UserCredentialsEntity validCredentialsEntityV1;


    private ExistingUserCredentialsModelWeb validAdminModelV3;

    private UserCredentialsEntity validCredentialsAdminEntityV3;

    private UserCredentialsEntity validCredentialsAdminEntityV1;


    @BeforeAll
    public void setUp() {
        dictionaryMappingService = new DictionaryMappingService(customerTypeRepository, userRoleRepository);
        userProfileMapper = new UserProfileMapperImpl(dictionaryMappingService);
        userCredentialsMapper = new UserCredentialsMapperImpl(dictionaryMappingService, userProfileMapper);
        updateUserService = new UpdateUserService(userCredentialsRepository,checkUserService, userCredentialsMapper);
        TestResourcesReader resourcesReader = new TestResourcesReader();

        userRoleEntity = (UserRoleEntity) resourcesReader.readData("Dictionaries/UserRoleEntityV1.json", UserRoleEntity.class);
        customerTypeEntity = (CustomerTypeEntity) resourcesReader.readData("Dictionaries/CustomerTypeEntityV1.json", CustomerTypeEntity.class);
        adminRoleEntity = (UserRoleEntity) resourcesReader.readData("Dictionaries/AdminRoleEntityV1.json", UserRoleEntity.class);
        customerTypeNoneEntity = (CustomerTypeEntity) resourcesReader.readData("Dictionaries/CustomerTypeNullV1.json", CustomerTypeEntity.class);

        validUserModelV3 = (ExistingUserCredentialsModelWeb) resourcesReader.readData("ExistingUserModel/ExistingValidUserModelV3.json", ExistingUserCredentialsModelWeb.class);
        validCredentialsEntityV3 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidUserEntityV3.json", UserCredentialsEntity.class);
        validCredentialsEntityV1 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidUserEntityV1.json", UserCredentialsEntity.class);

        validAdminModelV3 = (ExistingUserCredentialsModelWeb) resourcesReader.readData("ExistingUserModel/ExistingValidAdminModelV1.json", ExistingUserCredentialsModelWeb.class);
        validCredentialsAdminEntityV3 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidAdminEntityV3.json", UserCredentialsEntity.class);
        validCredentialsAdminEntityV1 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidAdminEntityV1.json", UserCredentialsEntity.class);
    }

    @Test
    void updateValidUser() {
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validUserModelV3.getLogin())).thenReturn(true);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validUserModelV3.getPhone())).thenReturn(true);
        Mockito.when(userRoleRepository.findById(1)).thenReturn(Optional.ofNullable(userRoleEntity));
        Mockito.when(customerTypeRepository.findById(1)).thenReturn(Optional.ofNullable(customerTypeEntity));
        Mockito.when(userCredentialsRepository.findById(validUserModelV3.getId())).thenReturn(Optional.ofNullable(validCredentialsEntityV1));
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validCredentialsEntityV3);
        var updatedModel = updateUserService.updateUserCredentials(validUserModelV3);
        verify(userCredentialsRepository).save(validCredentialsEntityV3);
        assertEquals(1, updatedModel.getId());
        assertEquals("ivan", updatedModel.getLogin());
        assertEquals("ivvanov@mail.ru", updatedModel.getEmail());
        assertNotNull(updatedModel.getUserProfile());
        assertNull(updatedModel.getUserProfile().getAddress());
        assertEquals(1, updatedModel.getUserProfile().getId());
        assertEquals(UserRoleModel.CUSTOMER, updatedModel.getUserRole());
        assertEquals(RETAIL, updatedModel.getUserProfile().getCustomerType());
    }


    @Test
    void updateValidAdminUser() {
        Mockito.when(checkUserService.isUserCredentialsExistsByLogin(validAdminModelV3.getLogin())).thenReturn(true);
        Mockito.when(checkUserService.isUserCredentialsExistsByPhone(validAdminModelV3.getPhone())).thenReturn(true);
        Mockito.when(userRoleRepository.findById(3)).thenReturn(Optional.ofNullable(adminRoleEntity));
        Mockito.when(customerTypeRepository.findById(4)).thenReturn(Optional.ofNullable(customerTypeNoneEntity));
        Mockito.when(userCredentialsRepository.findById(validAdminModelV3.getId())).thenReturn(Optional.ofNullable(validCredentialsAdminEntityV1));
        Mockito.when(userCredentialsRepository.save(Mockito.any(UserCredentialsEntity.class))).thenReturn(validCredentialsAdminEntityV3);
        var updatedModel = updateUserService.updateAdminCredentials(validAdminModelV3);
        verify(userCredentialsRepository).save(validCredentialsAdminEntityV3);
        assertEquals(2, updatedModel.getId());
        assertEquals("petr", updatedModel.getLogin());
        assertEquals("petrpetrpetr@gmail.com", updatedModel.getEmail());
        assertNotNull(updatedModel.getUserProfile());
        assertNull(updatedModel.getUserProfile().getAddress());
        assertEquals(2, updatedModel.getUserProfile().getId());
        assertEquals(UserRoleModel.ADMIN, updatedModel.getUserRole());
        assertEquals(NOT_CUSTOMER, updatedModel.getUserProfile().getCustomerType());
    }

}
