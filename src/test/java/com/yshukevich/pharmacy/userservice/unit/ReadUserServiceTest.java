package com.yshukevich.pharmacy.userservice.unit;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRolePermissionsEntity;
import com.yshukevich.pharmacy.userservice.exceptions.UserProfileNotFoundException;
import com.yshukevich.pharmacy.userservice.mapper.DictionaryMappingService;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserCredentialsMapperImpl;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserProfileMapperImpl;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsModel;
import com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel;
import com.yshukevich.pharmacy.userservice.models.dictionaries.UserRoleModel;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.CustomerTypeRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.UserRolePermissionsRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.UserRoleRepository;
import com.yshukevich.pharmacy.userservice.service.ReadUserService;
import com.yshukevich.pharmacy.userservice.util.TestResourcesReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(SpringExtension.class)
class ReadUserServiceTest {

    private UserCredentialsModel validUserModelV1;

    private UserCredentialsFullModel validUserModelV2;

    private UserCredentialsEntity validCredentialsEntityV1;

    private UserCredentialsEntity validCredentialsEntityV2;

    private List<UserRolePermissionsEntity> permissionsEntities;

    @InjectMocks
    private ReadUserService readUserService;

    @InjectMocks
    private UserCredentialsMapperImpl userCredentialsMapper;

    @InjectMocks
    private UserProfileMapperImpl userProfileMapper;

    @InjectMocks
    private DictionaryMappingService dictionaryMappingService;

    @Mock
    private UserCredentialsRepository userCredentialsRepository;

    @Mock
    private CustomerTypeRepository customerTypeRepository;

    @Mock
    private UserRoleRepository userRoleRepository;

    @Mock
    private UserRolePermissionsRepository rolePermissionsRepository;


    @BeforeAll
    public void setUp() {
        dictionaryMappingService = new DictionaryMappingService(customerTypeRepository, userRoleRepository);
        userProfileMapper = new UserProfileMapperImpl(dictionaryMappingService);
        userCredentialsMapper = new UserCredentialsMapperImpl(dictionaryMappingService, userProfileMapper);
        readUserService = new ReadUserService(userCredentialsRepository, rolePermissionsRepository, userCredentialsMapper);
        TestResourcesReader resourcesReader = new TestResourcesReader();
        permissionsEntities =  (List<UserRolePermissionsEntity>) resourcesReader.readDataList("Dictionaries/UserRolePermissionsV1.json", UserRolePermissionsEntity.class);
        validUserModelV1 = (UserCredentialsModel) resourcesReader.readData("ExistingUserModel/ExistingValidUserModelV1.json", UserCredentialsModel.class);
        validUserModelV2 = (UserCredentialsFullModel) resourcesReader.readData("ExistingUserModel/ExistingValidUserModelV2.json", UserCredentialsFullModel.class);
        validCredentialsEntityV1 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidUserEntityV1.json", UserCredentialsEntity.class);
        validCredentialsEntityV2 = (UserCredentialsEntity) resourcesReader.readData("ExistingUserEntity/ExistingValidUserEntityV2.json", UserCredentialsEntity.class);
    }


    @Test
    void readExistingUserById() {
        Mockito.when(userCredentialsRepository.findById(validUserModelV1.getId()))
                .thenReturn(Optional.of(validCredentialsEntityV1));
        var expected = readUserService.getUserCredentialsById(validUserModelV1.getId());
        assertEquals(1, expected.getId());
        assertEquals("12314", expected.getPhone());
        assertEquals(UserRoleModel.CUSTOMER, expected.getUserRole());
        assertNull(expected.getEmail());
    }

    @Test
    void readExistingUserByLogin() {
        Mockito.when(userCredentialsRepository.findByLogin(validUserModelV1.getLogin()))
                .thenReturn(Optional.of(validCredentialsEntityV1));
        var expected = readUserService.getUserCredentialsByLogin(validUserModelV1.getLogin());
        assertEquals(1, expected.getId());
        assertEquals("12314", expected.getPhone());
        assertEquals(UserRoleModel.CUSTOMER, expected.getUserRole());
        assertNull(expected.getEmail());
    }

    @Test
    void readExistingUserFullProfileByLogin() {
        Mockito.when(userCredentialsRepository.findByLogin(validUserModelV2.getLogin()))
                .thenReturn(Optional.of(validCredentialsEntityV2));
        var expected = readUserService.getFullUserCredentialsByLogin(validUserModelV2.getLogin());
        assertEquals(1, expected.getId());
        assertEquals("12314", expected.getPhone());
        assertEquals(UserRoleModel.CUSTOMER, expected.getUserRole());
        assertEquals(CustomerTypeModel.RETAIL, expected.getUserProfile().getCustomerType());
        assertEquals(1, expected.getUserProfile().getId());
        assertEquals("Ivanov", expected.getUserProfile().getSurname());
        assertNull(expected.getEmail());
    }

    @Test
    void mapUserRoleToPermissions(){
        Set<UserRolePermissionsEntity> permissions = new HashSet<>(permissionsEntities);
        Mockito.when(rolePermissionsRepository.getUserRolePermissionsEntitiesByUserRoleEntityName("Customer"))
                .thenReturn(Optional.of(permissions));
        var expected = readUserService.getUserPermissionsByUserRole("Customer");
        assertEquals(4, expected.size());
        assertTrue( expected.contains("Write a single user"));
        assertTrue( expected.contains("Read a single user"));
        assertTrue( expected.contains("Delete a single user"));
        assertTrue( expected.contains("Edit a single user"));
    }


    @Test
    void readNonExistingProfileByLogin() {
        Mockito.when(userCredentialsRepository.findByLogin(validUserModelV2.getLogin()))
                .thenReturn(Optional.empty());
        assertThrows(
                UserProfileNotFoundException.class,
                () -> readUserService.getFullUserCredentialsByLogin(validUserModelV2.getLogin())
        );
    }

    @Test
    void readNonExistingProfileById() {
        Mockito.when(userCredentialsRepository.findById(validUserModelV2.getId()))
                .thenReturn(Optional.empty());
        assertThrows(
                UserProfileNotFoundException.class,
                () -> readUserService.getUserCredentialsById(validUserModelV2.getId())
        );
    }
}
