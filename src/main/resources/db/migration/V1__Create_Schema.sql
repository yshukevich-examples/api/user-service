CREATE SCHEMA IF NOT EXISTS PHARMACY;
SET search_path = PHARMACY;

CREATE TABLE customer_type
(
    customer_type_id int          NOT NULL PRIMARY KEY,
    name             varchar(255) NOT NULL UNIQUE
);


CREATE TABLE user_permission
(
    user_permission_id int          NOT NULL PRIMARY KEY,
    name               varchar(255) NOT NULL UNIQUE
);

CREATE TABLE user_role
(
    user_role_id int          NOT NULL PRIMARY KEY,
    name         varchar(255) NOT NULL UNIQUE
);

CREATE TABLE user_role_permission
(
    id              SERIAL PRIMARY KEY,
    user_role       varchar(255) references user_role (name) NOT NULL,
    user_permission varchar(255) REFERENCES user_permission (name) NOT NULL
);


CREATE TABLE user_profile
(
    id                SERIAL         PRIMARY KEY,
    created           timestamp      NOT NULL default current_timestamp,
    updated           timestamp      NOT NULL default current_timestamp,
    firstname         varchar(255)   DEFAULT NULL,
    surname           varchar(255)   DEFAULT NULL,
    birthdate         date           DEFAULT NULL,
    country           varchar(64)    DEFAULT NULL,
    city              varchar(64)    DEFAULT NULL,
    address           varchar(255)   DEFAULT NULL,
    index             varchar(32)    DEFAULT NULL,
    company           varchar(255)   DEFAULT NULL,
    personal_discount decimal(19, 2) DEFAULT NULL,
    customer_type     varchar(255)   NOT NULL,
    version           int            NOT NULL,
    FOREIGN KEY (customer_type) REFERENCES customer_type (name)
);

CREATE TABLE user_credentials
(
    id        SERIAL          PRIMARY KEY,
    created   timestamp       NOT NULL default current_timestamp,
    updated   timestamp       NOT NULL default current_timestamp,
    phone     varchar(255)    NOT NULL UNIQUE,
    login     varchar(64)     NOT NULL UNIQUE,
    email     varchar(255)    DEFAULT NULL,
    password  varchar(255)    NOT NULL UNIQUE,
    user_role varchar(255)    NOT NULL,
    is_active boolean         NOT NULL,
    version   int             NOT NULL,
    user_profile_id int       NOT NULL,
    FOREIGN KEY (user_role) REFERENCES user_role (name)
);

