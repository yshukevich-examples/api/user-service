package com.yshukevich.pharmacy.userservice.models.dictionaries;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserPermissionModel {
    WRITE_ALL(1, "Create any user"),
    READ_ALL(2, "Read any user"),
    EDIT_ALL(3, "Edit any user"),
    DELETE_ALL(4, "Delete any user"),
    WRITE_ONE(5, "Write a single user"),
    READ_ONE(6, "Read a single user"),
    EDIT_ONE(7, "Edit a single user"),
    DELETE_ONE(8, "Delete a single user");

    private final Integer id;
    private final String name;
}
