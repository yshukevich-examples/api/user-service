package com.yshukevich.pharmacy.userservice.models.dictionaries;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserStatusModel {
    ACTIVE(1, "Active user"),
    BANNED(2, "Banned user");

    private final Integer id;
    private final String name;
}
