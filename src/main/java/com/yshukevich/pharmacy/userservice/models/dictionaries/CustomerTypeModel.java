package com.yshukevich.pharmacy.userservice.models.dictionaries;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CustomerTypeModel {
    RETAIL(1, "Customer as a person"),
    PARTNER(2, "Customer as a company"),
    NOT_CUSTOMER(3, "Not a Customer"),
    NONE (4, "No type");

    private final Integer id;
    private final String name;
}
