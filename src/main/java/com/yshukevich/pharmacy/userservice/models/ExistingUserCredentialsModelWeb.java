package com.yshukevich.pharmacy.userservice.models;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class ExistingUserCredentialsModelWeb {

        @NotNull
        Integer id;
        @NotNull
        @NotBlank
        String phone;
        @NotNull
        @NotBlank
        String login;

        @Email
        @NotNull
        @NotBlank
        String email;
        ExistingUserProfileModelWeb userProfile;
}
