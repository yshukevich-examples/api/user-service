package com.yshukevich.pharmacy.userservice.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yshukevich.pharmacy.userservice.models.dictionaries.UserRoleModel;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class UserCredentialsModel {
        @NotNull
        Integer id;
        @NotNull
        @NotBlank
        String phone;
        @NotNull
        @NotBlank
        String login;
        @NotNull
        @NotBlank
        @JsonIgnore
        String password;
        @Email
        String email;
        @NotNull
        UserRoleModel userRole;
        @NotNull
        Boolean isActive;

}
