package com.yshukevich.pharmacy.userservice.models.security;

import com.yshukevich.pharmacy.userservice.models.NewUserProfileModelWeb;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class NewUserCredentialsModelWeb {
        @NotNull
        @NotBlank
        String phone;
        @NotNull
        @NotBlank
        String login;
        @NotNull
        @NotBlank
        String password;
        @Email
        String email;
        NewUserProfileModelWeb userProfile;
}
