package com.yshukevich.pharmacy.userservice.models.dictionaries;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

@AllArgsConstructor
@Getter
public enum UserRoleModel {
    CUSTOMER(1, "Customer",
            Set.of(UserPermissionModel.READ_ONE,
                    UserPermissionModel.READ_ALL,
                    UserPermissionModel.DELETE_ONE,
                    UserPermissionModel.WRITE_ONE,
                    UserPermissionModel.EDIT_ONE)),
    MANAGER(2, "Sales manager",
            Set.of(UserPermissionModel.READ_ONE,
                    UserPermissionModel.READ_ALL,
                    UserPermissionModel.DELETE_ONE,
                    UserPermissionModel.WRITE_ONE,
                    UserPermissionModel.EDIT_ONE)),
    ADMIN(3, "Administrator",
            Set.of(UserPermissionModel.WRITE_ALL,
                    UserPermissionModel.EDIT_ALL,
                    UserPermissionModel.DELETE_ALL,
                    UserPermissionModel.READ_ALL));

    private final Integer id;
    private final String name;
    private final Set<UserPermissionModel> permissions;
}
