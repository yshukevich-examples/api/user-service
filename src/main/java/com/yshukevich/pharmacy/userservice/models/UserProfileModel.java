package com.yshukevich.pharmacy.userservice.models;

import com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.*;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@Jacksonized
public class UserProfileModel{
        @NotNull
        Integer id;
        String firstname;
        String surname;
        LocalDate birthdate;
        String country;
        String city;
        String address;
        String index;
        String company;
        @Positive
        BigDecimal personalDiscount;
        @NotNull
        CustomerTypeModel customerType;

}
