package com.yshukevich.pharmacy.userservice.models;

import com.yshukevich.pharmacy.userservice.models.dictionaries.UserRoleModel;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Data
@Builder
@Jacksonized
public class NewUserCredentialsModel {
        @NotNull
        @NotBlank
        String phone;
        @NotNull
        @NotBlank
        String login;
        @NotNull
        @NotBlank
        String password;
        @NotNull
        Boolean isActive;
        @Email
        String email;
        UserRoleModel userRole;
        NewUserProfileModel userProfile;
}
