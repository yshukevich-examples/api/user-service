package com.yshukevich.pharmacy.userservice.models;

import jakarta.validation.constraints.Positive;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@Jacksonized
public class NewUserProfileModelWeb {
        String firstname;
        String surname;
        LocalDate birthdate;
        String country;
        String city;
        String address;
        String index;
        String company;
        @Positive
        BigDecimal personalDiscount;

}
