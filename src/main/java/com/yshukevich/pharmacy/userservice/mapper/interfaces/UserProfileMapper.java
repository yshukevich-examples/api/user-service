package com.yshukevich.pharmacy.userservice.mapper.interfaces;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.mapper.DictionaryMappingService;
import com.yshukevich.pharmacy.userservice.entities.UserProfileEntity;
import com.yshukevich.pharmacy.userservice.models.*;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = DictionaryMappingService.class,
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface UserProfileMapper {
    UserProfileMapper instance = Mappers.getMapper(UserProfileMapper.class);

    UserProfileEntity toUserProfileEntity(UserProfileModel userProfileModel);

    UserProfileEntity toUserProfile(NewUserProfileModel userProfileModel);

    UserProfileModel toUserProfileModel(UserProfileEntity userProfileEntity);

    NewUserProfileModel toNewUserProfileModel(NewUserProfileModelWeb newUserProfileModel);

    UserProfileEntity toUserProfileEntity(ExistingUserProfileModelWeb userProfile);

    @BeanMapping(nullValuePropertyMappingStrategy =  NullValuePropertyMappingStrategy.IGNORE)
    UserProfileEntity toUserProfileEntity(@MappingTarget UserProfileEntity entity, ExistingUserProfileModelWeb userProfile);


}
