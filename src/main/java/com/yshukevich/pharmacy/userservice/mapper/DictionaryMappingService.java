package com.yshukevich.pharmacy.userservice.mapper;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.CustomerTypeEntity;
import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRoleEntity;
import com.yshukevich.pharmacy.userservice.exceptions.CustomerTypeNotFoundException;
import com.yshukevich.pharmacy.userservice.exceptions.UserRoleNotFoundException;
import com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel;
import com.yshukevich.pharmacy.userservice.models.dictionaries.UserRoleModel;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.CustomerTypeRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@Component
@RequiredArgsConstructor
public class DictionaryMappingService  {

    private final CustomerTypeRepository customerTypeRepository;

    private final UserRoleRepository userRoleRepository;

    @SneakyThrows
    public CustomerTypeModel toCustomerTypeModel(CustomerTypeEntity customerTypeEntity) {
        return Arrays.stream(CustomerTypeModel.values())
                .filter(customerTypeEnum -> customerTypeEnum.getId().equals(customerTypeEntity.getId()))
                .reduce((a, b) -> a)
                .orElseThrow(() -> new CustomerTypeNotFoundException("No Customer Type found for this entity " + customerTypeEntity));
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public CustomerTypeEntity toCustomerTypeEntity(CustomerTypeModel customerTypeEnum) {
        return customerTypeRepository
                .findById(customerTypeEnum.getId())
                .orElseThrow(() -> new CustomerTypeNotFoundException("No Customer Type found for this enum " + customerTypeEnum));
    }


    @SneakyThrows
    public UserRoleModel toUserRoleModel(UserRoleEntity userRoleEntity) {
        return Arrays.stream(UserRoleModel.values())
                .filter(userRoleEnum -> userRoleEnum.getId().equals(userRoleEntity.getId()))
                .reduce((a, b) -> a)
                .orElseThrow(() -> new UserRoleNotFoundException("No User Role found for this entity" + userRoleEntity));
    }



    @SneakyThrows
    @Transactional(readOnly = true)
    public UserRoleEntity toUserRoleEntity(UserRoleModel userRoleEnum) {
        return userRoleRepository.findById(userRoleEnum.getId())
                .orElseThrow(() -> new UserRoleNotFoundException("No User Role found for this enum" + userRoleEnum));
    }
}
