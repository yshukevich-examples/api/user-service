package com.yshukevich.pharmacy.userservice.mapper.interfaces;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.mapper.DictionaryMappingService;
import com.yshukevich.pharmacy.userservice.models.ExistingUserCredentialsModelWeb;
import com.yshukevich.pharmacy.userservice.models.NewUserCredentialsModel;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsModel;
import com.yshukevich.pharmacy.userservice.models.security.NewUserCredentialsModelWeb;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = {DictionaryMappingService.class, UserProfileMapper.class},
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValueMappingStrategy = NullValueMappingStrategy.RETURN_NULL)
public interface UserCredentialsMapper {

    UserCredentialsMapper instance = Mappers.getMapper(UserCredentialsMapper.class);

    UserCredentialsEntity toUserCredentialsEntity(UserCredentialsModel userCredentialsModel);

    UserCredentialsEntity toNewUserCredentialsEntity(NewUserCredentialsModel userCredentialsModel);

    UserCredentialsModel toUserCredentialsModel(UserCredentialsEntity userCredentialsEntity);

    UserCredentialsFullModel toUserCredentialsFullModel(UserCredentialsEntity userCredentialsEntity);

    NewUserCredentialsModel toNewUserCredentialsModel(NewUserCredentialsModelWeb model);

    @BeanMapping(nullValuePropertyMappingStrategy =  NullValuePropertyMappingStrategy.IGNORE)
    UserCredentialsEntity toUserCredentialsEntity(@MappingTarget UserCredentialsEntity entity, ExistingUserCredentialsModelWeb userProfile);



}
