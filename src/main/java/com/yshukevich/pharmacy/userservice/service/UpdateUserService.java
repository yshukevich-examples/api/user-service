package com.yshukevich.pharmacy.userservice.service;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.exceptions.UserProfileNotFoundException;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserCredentialsMapper;
import com.yshukevich.pharmacy.userservice.models.*;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UpdateUserService {

    private final UserCredentialsRepository userCredentialsRepository;
    private final CheckUserService checkUserService;
    private final UserCredentialsMapper userCredentialsMapper;


    @SneakyThrows
    @Transactional
    public UserCredentialsFullModel updateUserCredentials(ExistingUserCredentialsModelWeb newCredentials) {
        //TODO check login in jwt token, user may update itself only
        validateUser(newCredentials);
        log.info("User %s will be updated".formatted(newCredentials.toString()));
        UserCredentialsEntity updatedEntity = findEntityMapForUpdate(newCredentials);
        log.info("User %s1 is mapped to %s2".formatted(newCredentials.toString(), updatedEntity.toString()));
        var entity = userCredentialsRepository.save(updatedEntity);
        return userCredentialsMapper.toUserCredentialsFullModel(entity);

    }

    @SneakyThrows
    @Transactional
    public UserCredentialsFullModel updateAdminCredentials(ExistingUserCredentialsModelWeb existingCredentials) {
        validateUser(existingCredentials);
        log.info("Admin %s will be updated".formatted(existingCredentials.toString()));
        UserCredentialsEntity updatedEntity = findEntityMapForUpdate(existingCredentials);
        log.info("Admin %s1 is mapped to %s2".formatted(existingCredentials.toString(), updatedEntity.toString()));
        var entity = userCredentialsRepository.save(updatedEntity);
        return userCredentialsMapper.toUserCredentialsFullModel(entity);
    }

    @SneakyThrows
    private UserCredentialsEntity findEntityMapForUpdate(ExistingUserCredentialsModelWeb existingCredentials)  {
        UserCredentialsEntity existedEntity = userCredentialsRepository
                .findById(existingCredentials.getId())
                .orElseThrow(() -> new UserProfileNotFoundException("User Profile not found by id" + existingCredentials.getId()));
        log.info("Found user " + existedEntity);
        return userCredentialsMapper.toUserCredentialsEntity(existedEntity,existingCredentials);
    }

    @SneakyThrows
    private void validateUser(ExistingUserCredentialsModelWeb existingCredentials){
        if (!checkUserService.isUserCredentialsExistsByLogin(existingCredentials.getLogin())
                || !checkUserService.isUserCredentialsExistsByPhone(existingCredentials.getPhone())) {
            throw new UserProfileNotFoundException("No such user for update " + existingCredentials);
        }
    }
}
