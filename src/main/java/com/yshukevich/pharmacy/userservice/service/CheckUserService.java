package com.yshukevich.pharmacy.userservice.service;

import com.yshukevich.pharmacy.userservice.exceptions.UserRoleNotFoundException;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import com.yshukevich.pharmacy.userservice.repository.UserProfileRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CheckUserService {

    private final UserCredentialsRepository userCredentialsRepository;

    private final UserProfileRepository userProfileRepository;

    @Transactional(readOnly = true)
    public Boolean isUserProfileExistsById(Integer userID) {
        log.info("Checking profile by id %d".formatted(userID));
        return userProfileRepository.existsById(userID);
    }

    @Transactional(readOnly = true)
    public Boolean isUserCredentialsExistsByLogin(String login) {
        log.info("Checking profile by login %s".formatted(login));
        return userCredentialsRepository.existsUserCredentialsEntityByLogin(login);
    }

    @Transactional(readOnly = true)
    public Boolean isUserCredentialsExistsByPhone(String phone) {
        log.info("Checking profile by phone %s".formatted(phone));
        return userCredentialsRepository.existsUserCredentialsEntityByPhone(phone);
    }


    @SneakyThrows
    @Transactional(readOnly = true)
    public String getUserRoleById(Integer userID) {
        log.info("Checking user role by user id %d".formatted(userID));
        var entity = userCredentialsRepository.findById(userID);
        if( entity.isPresent()){
            return entity.get().getUserRole().getName();
        }
        else throw new UserRoleNotFoundException("No User Role found for user id"+ userID);
    }


}
