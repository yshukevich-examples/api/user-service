package com.yshukevich.pharmacy.userservice.service;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRolePermissionsEntity;
import com.yshukevich.pharmacy.userservice.exceptions.UserProfileNotFoundException;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserCredentialsMapper;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsModel;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import com.yshukevich.pharmacy.userservice.repository.dictionaries.UserRolePermissionsRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReadUserService {

    private final UserCredentialsRepository userCredentialsRepository;
    private final UserRolePermissionsRepository rolePermissionsRepository;
    private final UserCredentialsMapper userCredentialsMapper;

    @SneakyThrows
    @Transactional(readOnly = true)
    public UserCredentialsModel getUserCredentialsById(Integer userID) {
        return userCredentialsMapper
                .toUserCredentialsModel(userCredentialsRepository.findById(userID)
                        .orElseThrow(() -> new UserProfileNotFoundException("No User Profile found by user id" + userID)));
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public UserCredentialsModel getUserCredentialsByLogin(String login) {
        return userCredentialsMapper
                .toUserCredentialsModel(userCredentialsRepository.findByLogin(login)
                        .orElseThrow(() -> new UserProfileNotFoundException("No User Profile found by user login" + login)));
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public UserCredentialsFullModel getFullUserCredentialsByLogin(String login) {
        return userCredentialsMapper
                .toUserCredentialsFullModel(userCredentialsRepository.findByLogin(login)
                        .orElseThrow(() -> new UserProfileNotFoundException("No User Credentials found by user login" + login)));
    }

    @SneakyThrows
    @Transactional(readOnly = true)
    public Set<String> getUserPermissionsByUserRole(String userRole) {
        Set<UserRolePermissionsEntity> userRolePermissions = rolePermissionsRepository.getUserRolePermissionsEntitiesByUserRoleEntityName(userRole)
                .orElseThrow(() -> new UserProfileNotFoundException("No User Permissions found by user role" + userRole));
        log.info("Got user permissions " + userRolePermissions + "for user role" + userRole);
        return userRolePermissions
                .stream()
                .map(userRolePermissionsEntity -> userRolePermissionsEntity.getUserPermissionEntity().getName())
                .collect(Collectors.toSet());

    }



}
