package com.yshukevich.pharmacy.userservice.service;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.exceptions.UserProfileAlreadyExistsException;
import com.yshukevich.pharmacy.userservice.mapper.interfaces.UserCredentialsMapper;
import com.yshukevich.pharmacy.userservice.models.NewUserCredentialsModel;
import com.yshukevich.pharmacy.userservice.models.NewUserProfileModel;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.models.dictionaries.CustomerTypeModel;
import com.yshukevich.pharmacy.userservice.models.dictionaries.UserRoleModel;
import com.yshukevich.pharmacy.userservice.models.security.NewUserCredentialsModelWeb;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class CreateUserService {

    private final UserCredentialsRepository userCredentialsRepository;
    private final CheckUserService checkUserService;
    private final UserCredentialsMapper userCredentialsMapper;
    private final PasswordEncoder encoder;


    @SneakyThrows
    @Transactional
    public UserCredentialsFullModel createUserCredentials(NewUserCredentialsModelWeb newUserCredentialsModel) {
        if (checkUserService.isUserCredentialsExistsByLogin(newUserCredentialsModel.getLogin())
                || checkUserService.isUserCredentialsExistsByPhone(newUserCredentialsModel.getPhone())) {
            throw new UserProfileAlreadyExistsException("This user already exists " + newUserCredentialsModel);
        }
        log.info("User %s will be created".formatted(newUserCredentialsModel.toString()));
        var mappedCredentials = mapUserCredentialsModel(newUserCredentialsModel,
                CustomerTypeModel.RETAIL,
                UserRoleModel.CUSTOMER);
        mappedCredentials.setIsActive(true);
        mappedCredentials.setPassword(encoder.encode(newUserCredentialsModel.getPassword()));
        log.info("User %s1 is mapped to %s2".formatted(newUserCredentialsModel.toString(), mappedCredentials.toString()));
        UserCredentialsEntity newCredentials = userCredentialsMapper.toNewUserCredentialsEntity(mappedCredentials);
        var entity = userCredentialsRepository.save(newCredentials);
        return userCredentialsMapper.toUserCredentialsFullModel(entity);

    }

    @SneakyThrows
    @Transactional
    public UserCredentialsFullModel createAdminCredentials(NewUserCredentialsModelWeb newUserCredentialsModel) {
        if (checkUserService.isUserCredentialsExistsByLogin(newUserCredentialsModel.getLogin())
                || checkUserService.isUserCredentialsExistsByPhone(newUserCredentialsModel.getPhone())) {
            throw new UserProfileAlreadyExistsException("This admin user already exists " + newUserCredentialsModel);
        }
        log.info("Admin %s will be created".formatted(newUserCredentialsModel.toString()));
        var mappedCredentials = mapUserCredentialsModel(newUserCredentialsModel,
                CustomerTypeModel.NONE,
                UserRoleModel.ADMIN);
        mappedCredentials.setIsActive(true);
        mappedCredentials.setPassword(encoder.encode(newUserCredentialsModel.getPassword()));
        log.info("Admin %s1 is mapped to %s2".formatted(newUserCredentialsModel.toString(), mappedCredentials.toString()));
        UserCredentialsEntity newCredentials = userCredentialsMapper.toNewUserCredentialsEntity(mappedCredentials);
        var entity = userCredentialsRepository.save(newCredentials);
        return userCredentialsMapper.toUserCredentialsFullModel(entity);
    }

    private NewUserCredentialsModel mapUserCredentialsModel(NewUserCredentialsModelWeb newUserCredentialsModel,
                                                            CustomerTypeModel customerTypeModel,
                                                            UserRoleModel userRoleModel) {
        NewUserCredentialsModel model = userCredentialsMapper.toNewUserCredentialsModel(newUserCredentialsModel);

        model.setUserRole(userRoleModel);
        if (model.getUserProfile() != null) {
            model.getUserProfile().setCustomerType(customerTypeModel);
        } else {
            model.setUserProfile(NewUserProfileModel.builder().customerType(customerTypeModel).build());
        }
        return model;
    }
}
