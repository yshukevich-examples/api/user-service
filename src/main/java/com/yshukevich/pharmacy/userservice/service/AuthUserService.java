package com.yshukevich.pharmacy.userservice.service;

import com.yshukevich.pharmacy.userservice.models.UserCredentialsModel;
import com.yshukevich.pharmacy.userservice.models.security.LoginRequest;
import com.yshukevich.pharmacy.userservice.security.jwt.JwtUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthUserService {

    private final AuthenticationManager authenticationManager;
    private final ReadUserService readUserService;
    private final JwtUtils jwtUtils;

    public Map<String, String> authenticateUser(LoginRequest loginRequest) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        UserCredentialsModel user = readUserService.getUserCredentialsByLogin(loginRequest.getUsername());
        String token = jwtUtils.createToken(loginRequest.getUsername(), user.getUserRole().getName());
        Map<String, String> response = new HashMap<>();
        response.put("login", loginRequest.getUsername());
        response.put("token", token);
        return response;
    }
}
