package com.yshukevich.pharmacy.userservice.exceptions;

public class UserProfileNotFoundException extends Exception {

    public UserProfileNotFoundException(String message){
        super(message);
    }
}
