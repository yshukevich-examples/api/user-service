package com.yshukevich.pharmacy.userservice.exceptions;

public class UserRoleNotFoundException extends Exception {

    public UserRoleNotFoundException(String message){
        super(message);
    }
}
