package com.yshukevich.pharmacy.userservice.exceptions;

public class SavingUserException extends Exception {
    public SavingUserException(String message, Throwable cause){
        super(message, cause);
    }
}
