package com.yshukevich.pharmacy.userservice.exceptions;

public class UserProfileAlreadyExistsException extends Exception {

    public UserProfileAlreadyExistsException(String message){
        super(message);
    }
}
