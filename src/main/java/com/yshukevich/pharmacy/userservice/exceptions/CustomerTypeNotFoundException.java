package com.yshukevich.pharmacy.userservice.exceptions;

public class CustomerTypeNotFoundException extends Exception {

    public CustomerTypeNotFoundException(String message){
        super(message);
    }
}
