package com.yshukevich.pharmacy.userservice.security.services;

import com.yshukevich.pharmacy.userservice.models.UserCredentialsModel;
import com.yshukevich.pharmacy.userservice.security.PermissionsToAuthoritiesMapper;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;

@Data
@Component
@RequiredArgsConstructor
public class SecurityUser implements UserDetails {

    private final PermissionsToAuthoritiesMapper permissionsToAuthoritiesMapper;
    private String username;
    private String password;
    private List<SimpleGrantedAuthority> authorities;
    private boolean isActive;

    public UserDetails fromUser(UserCredentialsModel user) {
        return new org.springframework.security.core.userdetails.User(
                user.getLogin(),
                user.getPassword(),
                user.getIsActive(),
                user.getIsActive(),
                user.getIsActive(),
                user.getIsActive(),
                permissionsToAuthoritiesMapper.getAuthorities(user.getUserRole().getName())
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isActive;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isActive;
    }

    @Override
    public boolean isEnabled() {
        return isActive;
    }
}
