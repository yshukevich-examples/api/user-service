package com.yshukevich.pharmacy.userservice.security.services;


import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsModel;
import com.yshukevich.pharmacy.userservice.repository.UserCredentialsRepository;
import com.yshukevich.pharmacy.userservice.service.ReadUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Qualifier("pharmacyUserDetails")
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final SecurityUser securityUser;
    private final ReadUserService readUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserCredentialsModel user = readUserService.getUserCredentialsByLogin(username);
        log.info("Found user " + user);
        return securityUser.fromUser(user);
    }

}
