package com.yshukevich.pharmacy.userservice.security;

import com.yshukevich.pharmacy.userservice.service.ReadUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class PermissionsToAuthoritiesMapper {

    private final ReadUserService readUserService;

    public Set<SimpleGrantedAuthority> getAuthorities(String roleName) {
        Set<String> permissions = readUserService.getUserPermissionsByUserRole(roleName);
        log.info("For User Role " + roleName + " found " + permissions.toString() + " permissions");
        return permissions.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}
