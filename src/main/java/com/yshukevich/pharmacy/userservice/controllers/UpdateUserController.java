package com.yshukevich.pharmacy.userservice.controllers;

import com.yshukevich.pharmacy.userservice.models.ExistingUserCredentialsModelWeb;
import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.service.UpdateUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UpdateUserController {

    private final UpdateUserService updateUserService;

    @PatchMapping("/")
    @PreAuthorize("hasAuthority('Edit a single user')")
    public ResponseEntity<UserCredentialsFullModel> updateUser(@RequestBody @Valid ExistingUserCredentialsModelWeb userCredentials) {
        return ResponseEntity.ok(updateUserService.updateUserCredentials(userCredentials));
    }

}

