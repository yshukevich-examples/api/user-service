package com.yshukevich.pharmacy.userservice.controllers;

import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.models.security.NewUserCredentialsModelWeb;
import com.yshukevich.pharmacy.userservice.service.CreateUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin")
@CrossOrigin(origins = "*", maxAge = 3600)
public class CreateAdminController {

    private final CreateUserService createUserService;

    @PostMapping("/")
    @PreAuthorize("hasAuthority('Create any user')")
    public ResponseEntity<UserCredentialsFullModel> createUser(@RequestBody @Valid NewUserCredentialsModelWeb userCredentials) {
        return ResponseEntity.ok(createUserService.createAdminCredentials(userCredentials));
    }

}

