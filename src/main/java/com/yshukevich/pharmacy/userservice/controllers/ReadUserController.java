package com.yshukevich.pharmacy.userservice.controllers;

import com.yshukevich.pharmacy.userservice.models.UserCredentialsFullModel;
import com.yshukevich.pharmacy.userservice.service.ReadUserService;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ReadUserController {

    private final ReadUserService readUserService;


    @GetMapping("/getUserByLogin/{login}")
    @PreAuthorize("hasAuthority('Read a single user')")
    public ResponseEntity<UserCredentialsFullModel> getUser(@PathVariable @NotNull String login) {
        return ResponseEntity.ok(readUserService.getFullUserCredentialsByLogin(login));
    }

}

