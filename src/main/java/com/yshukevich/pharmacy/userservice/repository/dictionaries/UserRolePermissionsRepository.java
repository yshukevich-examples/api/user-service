package com.yshukevich.pharmacy.userservice.repository.dictionaries;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRolePermissionsEntity;
import jakarta.persistence.QueryHint;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;
import java.util.Set;

import static org.hibernate.jpa.HibernateHints.HINT_CACHEABLE;

public interface UserRolePermissionsRepository extends  CrudRepository<UserRolePermissionsEntity, Integer>,
        PagingAndSortingRepository<UserRolePermissionsEntity, Integer> {

    @QueryHints(@QueryHint(name = HINT_CACHEABLE, value = "true"))
    Optional<Set<UserRolePermissionsEntity>> getUserRolePermissionsEntitiesByUserRoleEntityName(String name);

}
