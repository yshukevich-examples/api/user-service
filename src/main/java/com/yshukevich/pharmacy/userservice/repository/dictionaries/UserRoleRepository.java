package com.yshukevich.pharmacy.userservice.repository.dictionaries;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRoleRepository extends  CrudRepository<UserRoleEntity, Integer>,
        PagingAndSortingRepository<UserRoleEntity, Integer> {
}
