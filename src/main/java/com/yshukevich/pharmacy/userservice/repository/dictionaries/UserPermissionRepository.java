package com.yshukevich.pharmacy.userservice.repository.dictionaries;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserPermissionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPermissionRepository extends  CrudRepository<UserPermissionEntity, Integer>,
        PagingAndSortingRepository<UserPermissionEntity, Integer> {

}
