package com.yshukevich.pharmacy.userservice.repository.dictionaries;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.CustomerTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerTypeRepository extends CrudRepository<CustomerTypeEntity, Integer>,
        PagingAndSortingRepository<CustomerTypeEntity, Integer> {
}
