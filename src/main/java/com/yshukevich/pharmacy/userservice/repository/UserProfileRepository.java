package com.yshukevich.pharmacy.userservice.repository;

import com.yshukevich.pharmacy.userservice.entities.UserProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserProfileRepository extends JpaRepository<UserProfileEntity, Integer>,
        PagingAndSortingRepository<UserProfileEntity, Integer> {



}
