package com.yshukevich.pharmacy.userservice.repository;

import com.yshukevich.pharmacy.userservice.entities.UserCredentialsEntity;
import jakarta.persistence.QueryHint;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

import static org.hibernate.jpa.HibernateHints.HINT_CACHEABLE;

public interface UserCredentialsRepository extends JpaRepository<UserCredentialsEntity, Integer>,
        PagingAndSortingRepository<UserCredentialsEntity, Integer> {

    @Lock(LockModeType.OPTIMISTIC)
    @QueryHints(@QueryHint(name = HINT_CACHEABLE, value = "true"))
    Optional<UserCredentialsEntity> findByLogin(String login);

    @Lock(LockModeType.OPTIMISTIC)
    @QueryHints(@QueryHint(name = HINT_CACHEABLE, value = "true"))
    Boolean existsUserCredentialsEntityByLogin(String login);

    @Override
    @Lock(LockModeType.OPTIMISTIC)
    @QueryHints(@QueryHint(name = HINT_CACHEABLE, value = "true"))
    UserCredentialsEntity save(UserCredentialsEntity entity);

    Boolean existsUserCredentialsEntityByPhone(String phone);




}
