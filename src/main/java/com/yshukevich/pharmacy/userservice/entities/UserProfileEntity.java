package com.yshukevich.pharmacy.userservice.entities;

import com.yshukevich.pharmacy.userservice.entities.dictionaries.CustomerTypeEntity;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "user_profile")
public class UserProfileEntity {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created", updatable = false)
    public LocalDateTime created;

    @Column(name = "updated", insertable = false)
    public LocalDateTime updated;

    @Column
    private String firstname;

    @Column
    private String surname;

    @Column
    private LocalDate birthdate;

    @Column
    private String country;

    @Column
    private String city;

    @Column
    private String address;

    @Column
    private String index;

    @Column
    private String company;

    @Version
    private Integer version;

    @Column
    private BigDecimal personalDiscount;

    @ManyToOne
    @JoinColumn(name = "customer_type", referencedColumnName = "name", nullable = false)
    private CustomerTypeEntity customerType;


    @PrePersist
    public void toCreate() {
        setCreated(LocalDateTime.now());
    }

    @PreUpdate
    public void toUpdate() {
        setUpdated(LocalDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserProfileEntity that)) return false;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getCreated(), that.getCreated()) && Objects.equals(getUpdated(), that.getUpdated()) && Objects.equals(getFirstname(), that.getFirstname()) && Objects.equals(getSurname(), that.getSurname()) && Objects.equals(getBirthdate(), that.getBirthdate()) && Objects.equals(getCountry(), that.getCountry()) && Objects.equals(getCity(), that.getCity()) && Objects.equals(getAddress(), that.getAddress()) && Objects.equals(getIndex(), that.getIndex()) && Objects.equals(getCompany(), that.getCompany()) && Objects.equals(getPersonalDiscount(), that.getPersonalDiscount())  && Objects.equals(getCustomerType(), that.getCustomerType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCreated(), getUpdated(), getFirstname(), getSurname(), getBirthdate(), getCountry(), getCity(), getAddress(), getIndex(), getCompany(), getPersonalDiscount(), getCustomerType());
    }

    @Override
    public String toString() {
        return "UserProfileEntity{" +
                "id=" + id +
                ", created=" + created +
                ", updated=" + updated +
                ", name='" + firstname + '\'' +
                ", surname='" + surname + '\'' +
                ", birthdate=" + birthdate +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", index='" + index + '\'' +
                ", company='" + company + '\'' +
                ", personalDiscount=" + personalDiscount +
                ", customerType=" + customerType +
                '}';
    }
}
