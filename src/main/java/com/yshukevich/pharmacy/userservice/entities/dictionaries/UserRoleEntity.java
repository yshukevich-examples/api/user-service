package com.yshukevich.pharmacy.userservice.entities.dictionaries;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@Immutable
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "user_role")
public class UserRoleEntity {

    @Id
    @Column(name = "user_role_id")
    protected Integer id;

    @Column(name = "name", unique = true)
    protected String name;

    @OneToMany(
            mappedBy = "userRoleEntity",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<UserRolePermissionsEntity> roleModels;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRoleEntity that)) return false;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getRoleModels(), that.getRoleModels());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getRoleModels());
    }

    @Override
    public String toString() {
        return "UserRoleEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", roleModels=" + roleModels +
                '}';
    }
}
