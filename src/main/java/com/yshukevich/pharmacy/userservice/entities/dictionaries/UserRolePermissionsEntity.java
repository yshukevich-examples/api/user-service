package com.yshukevich.pharmacy.userservice.entities.dictionaries;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

import java.util.Objects;

@Entity
@Getter
@Setter
@Immutable
@Table(name = "user_role_permission")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserRolePermissionsEntity {

    @Id
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_role", referencedColumnName = "name", nullable = false)
    private UserRoleEntity userRoleEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_permission", referencedColumnName = "name", nullable = false)
    private UserPermissionEntity userPermissionEntity;


    @Override
    public String toString() {
        return "UserRolePermissionsEntity{" +
                "id=" + id +
                ", userRoleEntity=" + userRoleEntity +
                ", userPermissionEntity=" + userPermissionEntity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserRolePermissionsEntity that)) return false;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getUserRoleEntity(), that.getUserRoleEntity()) && Objects.equals(getUserPermissionEntity(), that.getUserPermissionEntity());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserRoleEntity(), getUserPermissionEntity());
    }
}
