package com.yshukevich.pharmacy.userservice.entities.dictionaries;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

import java.util.Objects;
import java.util.Set;

@Entity
@Getter
@Setter
@Immutable
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "user_permission")
public class UserPermissionEntity  {

    @Id
    @Column(name = "user_permission_id")
    protected Integer id;

    @Column(name = "name")
    protected String name;

    @OneToMany(
            mappedBy = "userPermissionEntity",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private Set<UserRolePermissionsEntity> permissionsModels;

    @Override
    public String toString() {
        return "UserPermissionEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", permissionsModels=" + permissionsModels +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserPermissionEntity that)) return false;
        if (!super.equals(o)) return false;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects.equals(getPermissionsModels(), that.getPermissionsModels());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getId(), getName(), getPermissionsModels());
    }
}
