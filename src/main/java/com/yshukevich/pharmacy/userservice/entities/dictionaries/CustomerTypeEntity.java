package com.yshukevich.pharmacy.userservice.entities.dictionaries;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

import java.util.Objects;


@Entity
@Getter
@Setter
@Immutable
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "customer_type")
public class CustomerTypeEntity  {

    @Id
    @Column(name = "customer_type_id")
    protected Integer id;

    @Column(name = "name")
    protected String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CustomerTypeEntity that)) return false;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }

    @Override
    public String toString() {
        return "CustomerTypeEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
