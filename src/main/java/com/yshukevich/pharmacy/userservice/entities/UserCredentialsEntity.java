package com.yshukevich.pharmacy.userservice.entities;


import com.yshukevich.pharmacy.userservice.entities.dictionaries.UserRoleEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
@Entity
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "user_credentials")
public class UserCredentialsEntity {

    @Id
    @Column(name = "id", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "created", updatable = false)
    public LocalDateTime created;

    @Column(name = "updated", insertable = false)
    public LocalDateTime updated;

    @NotNull
    @Column(unique = true)
    public String phone;

    @NotNull
    @Column(unique = true, length = 64)
    private String login;

    @Column
    public String email;

    @NotNull
    @Column(unique = true)
    public String password;

    @NotNull
    @Column
    private Boolean isActive;

    @Version
    private Integer version;

    @ManyToOne
    @JoinColumn(name = "user_role", referencedColumnName = "name", nullable = false)
    private UserRoleEntity userRole;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private UserProfileEntity userProfile;


    @PrePersist
    public void toCreate() {
        setCreated(LocalDateTime.now());
    }

    @PreUpdate
    public void toUpdate() {
        setUpdated(LocalDateTime.now());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserCredentialsEntity that)) return false;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getCreated(), that.getCreated()) && Objects.equals(getUpdated(), that.getUpdated()) && Objects.equals(getPhone(), that.getPhone()) && Objects.equals(getLogin(), that.getLogin()) && Objects.equals(getEmail(), that.getEmail()) && Objects.equals(getPassword(), that.getPassword()) && Objects.equals(getIsActive(), that.getIsActive()) && Objects.equals(getUserRole(), that.getUserRole()) && Objects.equals(getUserProfile(), that.getUserProfile());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCreated(), getUpdated(), getPhone(), getLogin(), getEmail(), getPassword(), getIsActive(), getUserRole(), getUserProfile());
    }

    @Override
    public String toString() {
        return "UserCredentialsEntity{" +
                "id=" + id +
                ", created=" + created +
                ", updated=" + updated +
                ", phone='" + phone + '\'' +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", isActive=" + isActive +
                ", userRole=" + userRole +
                ", userProfile=" + userProfile +
                '}';
    }
}
